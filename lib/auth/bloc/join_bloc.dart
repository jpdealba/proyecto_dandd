import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

part 'join_event.dart';
part 'join_state.dart';

class JoinBloc extends Bloc<JoinEvent, JoinState> {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  JoinBloc() : super(JoinInitial()) {
    on<JoinWithCodeEvent>(_joinWithCode);
  }

  FutureOr<void> _joinWithCode(event, emit) async {
    var code = event.code;
    var userDoc = await FirebaseFirestore.instance.collection("game").doc(code);
    var docsRef = await userDoc.get();
    var resp = docsRef.data();

    if (docsRef.exists) {
      if (!resp!["players"].contains(_auth.currentUser!.uid)) {
        var queryGame =
            await FirebaseFirestore.instance.collection("game").doc("${code}");
        // query para sacar la data del documento
        var docsRef = await queryGame.get();
        List<dynamic> listIds = docsRef.data()?["players"];
        listIds.add(_auth.currentUser!.uid);
        print(listIds);
        await queryGame.update({"players": listIds});
      }
      var docsRef2 = await userDoc.get();
      var resp2 = docsRef2.data();
      emit(JoinSuccessState(game: resp2));
      emit(JoinInitial());
    } else {
      emit(JoinErrorState());
    }
  }
}
