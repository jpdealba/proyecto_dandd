import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:proyecto/LogIn/login.dart';
import 'package:proyecto/auth/bloc/join_bloc.dart';
import 'package:proyecto/auth/index.dart';

import 'LogIn/bloc/loginauth_bloc.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MultiBlocProvider(providers: [
    BlocProvider(
      create: (context) => LoginauthBloc()..add(VerifyAuthEvent()),
    ),
    BlocProvider(
      create: (context) => JoinBloc(),
    )
  ], child: Main()));
}

class Main extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(
          fontFamily: "Letter",
        ),
        title: 'D & D',
        home: BlocConsumer<LoginauthBloc, LoginauthState>(
            builder: ((context, state) {
          if (state is AuthSuccessState) {
            return LogScreen();
          } else if (state is UnAuthState ||
              state is AuthErrorState ||
              state is SignOutSuccessState) {
            return LoginPage();
          }
          return const Center(
            child: CircularProgressIndicator(),
          );
        }), listener: (context, state) {
          const SnackBar(
            content: Text("Favor de autenticarse"),
          );
        }));
  }
}
