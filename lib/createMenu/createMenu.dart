import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:numberpicker/numberpicker.dart';
import 'package:proyecto/my_presets.dart';
import 'package:proyecto/waitingScreen/waiting_screen.dart';

import 'bloc/picture_bloc.dart';

class CreateMenu extends StatefulWidget {
  CreateMenu({Key? key}) : super(key: key);

  @override
  State<CreateMenu> createState() => _CreateMenuState();
}

class _CreateMenuState extends State<CreateMenu> {
  final _titleController = TextEditingController();

  int _width = 5;
  int _height = 5;

  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Align(
          alignment: Alignment.center,
          child: Text(
            'Board Creator',
            style: appbar_text1(),
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height -
              get_appbar_size() -
              MediaQuery.of(context).viewPadding.top,
          decoration: container_style_1(),
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                TextField(
                  controller: _titleController,
                  inputFormatters: [LengthLimitingTextInputFormatter(18)],
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 35),
                  decoration: InputDecoration(
                      border: UnderlineInputBorder(), hintText: 'Board Title'),
                ),
                SizedBox(height: 25),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    BlocConsumer<PictureBloc, PictureState>(
                        builder: (context, state) {
                          if (state is PictureSelectedState) {
                            return picture_success(context, state.picture);
                          } else {
                            return picture_default(context);
                          }
                        },
                        listener: (context, state) {}),
                    Column(
                      children: [
                        sizes_text("Width"),
                        NumberPicker(
                            minValue: 3,
                            maxValue: 9,
                            value: _width,
                            haptics: true,
                            itemHeight: 25,
                            onChanged: (int newNum) {
                              _width = newNum;
                              setState(() {});
                            }),
                        SizedBox(height: 25),
                        sizes_text("Height"),
                        NumberPicker(
                            minValue: 3,
                            maxValue: 9,
                            value: _height,
                            haptics: true,
                            itemHeight: 25,
                            onChanged: (int newNum) {
                              _height = newNum;
                              setState(() {});
                            })
                      ],
                    )
                  ],
                ),
                SizedBox(
                  height: 30,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    ElevatedButton(
                        onPressed: () {
                          BlocProvider.of<PictureBloc>(context)
                              .add(ChangeImageEvent());
                        },
                        child: Text(
                          "Gallery",
                          style: TextStyle(
                              fontSize: 30, fontWeight: FontWeight.bold),
                        )),
                    ElevatedButton(
                        onPressed: () {
                          BlocProvider.of<PictureBloc>(context)
                              .add(ChangeImageCameraEvent());
                        },
                        child: Text(
                          "Camera",
                          style: TextStyle(
                              fontSize: 30, fontWeight: FontWeight.bold),
                        )),
                  ],
                ),
                SizedBox(height: 25),
                BlocConsumer<PictureBloc, PictureState>(
                  listener: (context, state) {
                    if (state is PostGameState) {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) =>
                              WaitingScreen(event_id: state.event_id)));
                    } else if (state is ErrorGameState) {
                      ScaffoldMessenger.of(context)
                        ..hideCurrentSnackBar()
                        ..showSnackBar(SnackBar(
                            content: Text("Error when posting the game")));
                    }
                  },
                  builder: (context, state) {
                    return ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          shape: CircleBorder(),
                          padding: EdgeInsets.all(50),
                        ),
                        onPressed: () {
                          if (state is LoadGameState) {
                          } else {
                            if (_titleController.text.isNotEmpty) {
                              BlocProvider.of<PictureBloc>(context).add(
                                  PostGameEvent(
                                      title: _titleController.text,
                                      width: "$_width",
                                      height: "$_height"));
                            } else {
                              ScaffoldMessenger.of(context)
                                ..hideCurrentSnackBar()
                                ..showSnackBar(SnackBar(
                                    content: Text("Complete Information")));
                            }
                          }
                        },
                        child: Text(
                          "Create!",
                          style: TextStyle(
                              fontSize: 35, fontWeight: FontWeight.bold),
                        ));
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Container picture_default(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage("assets/default_map.png"), fit: BoxFit.fill),
          border: Border.all(color: Colors.black, width: 5),
          borderRadius: BorderRadius.circular(20)),
      width: 200,
      height: 200,
    );
  }

  Container picture_success(BuildContext context, File image) {
    return Container(
      decoration: BoxDecoration(
          image: DecorationImage(image: FileImage(image), fit: BoxFit.fill),
          border: Border.all(color: Colors.black, width: 5),
          borderRadius: BorderRadius.circular(20)),
      width: 200,
      height: 200,
    );
  }

  Text sizes_text(String text) {
    return Text(text, style: TextStyle(fontSize: 30));
  }
}
