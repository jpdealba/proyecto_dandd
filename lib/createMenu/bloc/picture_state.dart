part of 'picture_bloc.dart';

abstract class PictureState extends Equatable {
  const PictureState();

  @override
  List<Object?> get props => [];
}

class PictureInitial extends PictureState {}

class PictureErrorState extends PictureState {
  final String errorMsg;

  PictureErrorState({required this.errorMsg});
  @override
  List<Object?> get props => [errorMsg];
}

class PictureSelectedState extends PictureState {
  final File picture;

  PictureSelectedState({required this.picture});
  @override
  List<Object?> get props => [picture];
}

class PostGameState extends PictureState {
  final String event_id;
  PostGameState({required this.event_id});
  @override
  List<Object?> get props => [event_id];
}

class LoadGameState extends PictureState {}

class ErrorGameState extends PictureState {}
