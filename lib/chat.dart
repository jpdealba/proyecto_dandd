import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:proyecto/my_presets.dart';
import 'dart:math' as math;

import 'custom_shape.dart';

class Chat extends StatefulWidget {
  final String code;
  Chat({Key? key, required this.code}) : super(key: key);

  @override
  State<Chat> createState() => _ChatState();
}

//change name to uid when firebase implemented
var messages = [
  {
    "name": "John",
    "photo":
        "https://richardgarcia.net/wp-content/uploads/2017/01/hombre-exitoso-png.png",
    "message":
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed pharetra est. Donec in consequat odio, vel euismod eros. Donec urna libero,"
  },
  {
    "name": "Sofia",
    "photo":
        "https://toppng.com/uploads/preview/mujeres-en-png-mujer-feliz-11563001923iicassndn8.png",
    "message":
        "hmolestie molestie magna ac, bibendum cursus ex. Suspendisse in nisi nisl. Duis quis tempor erat, ut lobortis enim. Sed eu velit velit. Donec ut jus"
  },
  {
    "name": "Fernando",
    "photo":
        "https://1.bp.blogspot.com/-xQjYgbOWBVo/V_mkuxzIJKI/AAAAAAAABOE/WiqjQQV15cAuwbXmtem26vywz4mTKlw8ACLcB/s1600/hombre.png",
    "message":
        "nt ante facilisis eu. Vestibulum ullamcorper ligula at magna dictum, ac malesuada dui mattis. Etiam a lacus convallis, cursus erat sit amet, fringilla purus. "
  },
  {
    "name": "Pedro",
    "photo":
        "https://1.bp.blogspot.com/-5ydU5haAbEY/V_mkYhkzOVI/AAAAAAAABOs/cVcd0cPB45AanuRdHIYoA7uYD5Wh1IkaACEw/s1600/banner2-man.png",
    "message":
        "as blandit massa nec leo vestibulum, a eleifend quam auctor. Vivamus felis purus, sollicitudin vel faucibus id, ornare sit amet elit. Nunc volutpat, purus sit amet s"
  },
  {
    "name": "John",
    "photo":
        "https://richardgarcia.net/wp-content/uploads/2017/01/hombre-exitoso-png.png",
    "message":
        "a fames ac ante ipsum primis in faucibus. Sed posuere justo vitae pellentesque condimentum. Duis sit amet augue a tortor feugiat gravida. Duis"
  },
  {
    "name": "username",
    "photo":
        "https://www.nicepng.com/png/detail/128-1280406_view-user-icon-png-user-circle-icon-png.png",
    "message":
        "a fames ac ante ipsum primis in faucibus. Sed posuere justo vitae pellentesque condimentum. Duis sit amet augue a tortor feugiat gravida. Duis"
  },
];

class _ChatState extends State<Chat> {
  var messageC = TextEditingController();
  final FirebaseAuth _auth = FirebaseAuth.instance;
  ScrollController controller = new ScrollController();
  double height = 0.0;
  double width = 60.0;

  void _sendMessage(message, event_id) async {
    var userDoc = await FirebaseFirestore.instance
        .collection("user")
        .doc(
          _auth.currentUser!.uid,
        )
        .get();

    var new_message = {
      "message": message,
      "photo_url": userDoc.data()!["photo_url"],
      "uid": _auth.currentUser!.uid,
      "username": userDoc.data()!["username"]
    };
    var chat_list =
        await FirebaseFirestore.instance.collection("chat").doc(event_id).get();
    var messages = chat_list.data()!["messages"];
    messages.add(new_message);
    await FirebaseFirestore.instance
        .collection("chat")
        .doc(event_id)
        .set({"messages": messages});
    messageC.clear();
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    if (controller.hasClients) {
      controller.jumpTo(controller.position.maxScrollExtent);
    }
  }

  @override
  Widget build(BuildContext context) {
    final FirebaseAuth _auth = FirebaseAuth.instance;
    Stream _chatStream = FirebaseFirestore.instance
        .collection('chat')
        .doc("${widget.code}")
        .snapshots(includeMetadataChanges: true);

    return Scaffold(
      appBar: AppBar(
        title: Align(
          alignment: Alignment.center,
          child: Text(
            "Chat",
            style: appbar_text1(),
          ),
        ),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.arrow_downward),
              onPressed: () {
                controller.jumpTo(controller.position.maxScrollExtent);
              }),
        ],
      ),
      body: StreamBuilder<dynamic>(
          stream: _chatStream,
          builder: (context, snapshot) {
            if (snapshot.hasError) {
              return Text('Something went wrong');
            }
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Center(
                child: CircularProgressIndicator(),
              );
            } else {
              var DocData = snapshot.data as DocumentSnapshot;
              return Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage("assets/pieces_blue.png"),
                      fit: BoxFit.cover),
                  color: Colors.white,
                ),
                child: Column(children: [
                  Flexible(
                    child: ListView.builder(
                      controller: controller,
                      itemCount: DocData["messages"].length,
                      itemBuilder: (BuildContext context, int index) {
                        if (DocData["messages"][index]["uid"] !=
                            _auth.currentUser!.uid)
                          return Column(children: [
                            Padding(
                              padding: const EdgeInsets.only(
                                  bottom: 10.0,
                                  left: 10.0,
                                  right: 50.0,
                                  top: 10.0),
                              child: Align(
                                alignment: Alignment.topLeft,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    CircleAvatar(
                                      backgroundColor: Colors.grey,
                                      radius: 20,
                                      backgroundImage: NetworkImage(
                                          '${DocData["messages"][index]["photo_url"]}'),
                                    ),
                                    Transform(
                                      alignment: Alignment.center,
                                      transform: Matrix4.rotationY(math.pi),
                                      child: CustomPaint(
                                        painter: CustomShape(Colors.amber),
                                      ),
                                    ),
                                    Flexible(
                                      child: Container(
                                        padding: EdgeInsets.all(14),
                                        decoration: BoxDecoration(
                                          color: Colors.amber,
                                          borderRadius: BorderRadius.only(
                                            topRight: Radius.circular(18),
                                            bottomLeft: Radius.circular(18),
                                            bottomRight: Radius.circular(18),
                                          ),
                                        ),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              '${DocData["messages"][index]["username"]}: ',
                                              style: TextStyle(
                                                  color: Colors.black54,
                                                  fontSize: 20),
                                            ),
                                            Text(
                                              '${DocData["messages"][index]["message"]}',
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 20),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ]);
                        else {
                          return Column(children: [
                            Padding(
                              padding: const EdgeInsets.only(
                                  bottom: 10.0,
                                  left: 50.0,
                                  right: 10.0,
                                  top: 10.0),
                              child: Align(
                                alignment: Alignment.topLeft,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Flexible(
                                        child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Flexible(
                                          child: Container(
                                            padding: EdgeInsets.all(14),
                                            decoration: BoxDecoration(
                                              color: Colors.lightGreen,
                                              borderRadius: BorderRadius.only(
                                                topLeft: Radius.circular(18),
                                                bottomLeft: Radius.circular(18),
                                                bottomRight:
                                                    Radius.circular(18),
                                              ),
                                            ),
                                            child: Text(
                                              '${DocData["messages"][index]["message"]}',
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 25),
                                            ),
                                          ),
                                        ),
                                        CustomPaint(
                                            painter:
                                                CustomShape(Colors.lightGreen)),
                                      ],
                                    )),
                                  ],
                                ),
                              ),
                            ),
                          ]);
                        }
                      },
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Padding(
                      padding: const EdgeInsets.only(
                          left: 15.0, right: 15.0, bottom: 5),
                      child: TextFormField(
                        style: TextStyle(fontSize: 25),
                        maxLength: 100,
                        controller: messageC,
                        decoration: InputDecoration(
                          border: UnderlineInputBorder(),
                          labelText: 'Say anything!',
                          labelStyle: TextStyle(fontSize: 30),
                          suffixIcon: IconButton(
                            onPressed: () {
                              _sendMessage(messageC.text, widget.code);
                            },
                            icon: Icon(Icons.send),
                          ),
                        ),
                      ),
                    ),
                  )
                ]),
              );
            }
          }),
    );
  }
}
