import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:proyecto/LogIn/bloc/loginauth_bloc.dart';
import 'package:proyecto/my_presets.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: container_style_2(),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Center(
                  child: Text(
                    "Sign In",
                    style: TextStyle(
                      fontSize: 75,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontFamily: "Letter",
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                MaterialButton(
                  child: Text(
                    "Start",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 60,
                      fontFamily: "Letter",
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  shape: CircleBorder(),
                  color: Colors.white,
                  padding: EdgeInsets.all(110),
                  onPressed: () {
                    BlocProvider.of<LoginauthBloc>(context)
                        .add(GoogleAuthEvent());
                  },
                ),
                SizedBox(
                  height: 100,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
