import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:proyecto/auth/index.dart';
import 'dart:convert';

import 'package:proyecto/chat.dart';

class GamePreview extends StatefulWidget {
  final String event_id;
  GamePreview({Key? key, required this.event_id}) : super(key: key);

  @override
  State<GamePreview> createState() => _GamePreviewState();
}

class _GamePreviewState extends State<GamePreview> {
  @override
  List<dynamic> users_in_game = [];
  List<String> ids = [];
  // Dummy info
  var user;

  String _currentPlayerId = FirebaseAuth.instance.currentUser!.uid;
  bool isPlayerDM = false;

  Map<String, dynamic> _playerList = {};
  String image =
      "https://firebasestorage.googleapis.com/v0/b/dnd-jpdealba.appspot.com/o/default_map.png?alt=media&token=48313438-32b0-4543-b471-ec83d5c779a8";
  double _currentScaleValue = 1.0;

  double pieceSize = 30.0;

  TransformationController _transformationController =
      TransformationController();
  @override
  void initState() {
    super.initState();
    List<dynamic> new_users_in_game = [];
    List<String> new_ids = [];
    FirebaseFirestore.instance
        .collection('game')
        .doc(widget.event_id)
        .snapshots()
        .listen(
      (DocumentSnapshot documentSnapshot) async {
        new_ids = [];
        new_users_in_game = [];
        for (var i = 0; i < documentSnapshot["players"].length; i++) {
          var queryPlayer = await FirebaseFirestore.instance
              .collection("user")
              .doc("${documentSnapshot["players"][i]}");
          var playerRef = await queryPlayer.get();
          if (!new_ids.contains(playerRef.data()?["uid"])) {
            new_users_in_game.add(playerRef.data());
            new_ids.add(playerRef.data()?["uid"]);
          }
          users_in_game = new_users_in_game;
          ids = new_ids;
        }
      },
    ).onError((e) => print(e));
    setState(() {});
  }

  Widget build(BuildContext context) {
    Stream _gameStream = FirebaseFirestore.instance
        .collection('game')
        .doc(widget.event_id)
        .snapshots(includeMetadataChanges: true);

    return StreamBuilder<dynamic>(
        stream: _gameStream,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Text('Something went wrong');
          }
          if (snapshot.connectionState == ConnectionState.waiting) {
            return GameMapWait(context, image, null);
          } else {
            var DocData = snapshot.data as DocumentSnapshot;
            image = DocData["map"];
            isPlayerDM =
                DocData["leader"] == FirebaseAuth.instance.currentUser!.uid
                    ? true
                    : false;
            return GameMap(context, DocData);
          }
        });
  }

  Scaffold GameMap(BuildContext context, var DocData) {
    Future<Map<String, dynamic>?> _asyncMethod(players) async {
      double width = MediaQuery.of(context).size.width;
      double height = MediaQuery.of(context).size.height;
      var queryUser = await FirebaseFirestore.instance
          .collection("user")
          .doc("${FirebaseAuth.instance.currentUser!.uid}");
      var docsRef = await queryUser.get();
      user = docsRef.data();

      for (var i = 0; i < players.length; i++) {
        var queryPlayer = await FirebaseFirestore.instance
            .collection("user")
            .doc("${players[i]}");
        var playerRef = await queryPlayer.get();
        if (!ids.contains(playerRef.data()?["uid"])) {
          users_in_game.add(playerRef.data());
          ids.add(playerRef.data()?["uid"]);
        }
        // obtener las coords de ese usuario
        _playerList["${playerRef.data()?["uid"]}"] = {
          "id": "${playerRef.data()?["uid"]}",
          "name": playerRef.data()?["username"],
          "next_user":
              "${i + 1 < players.length ? players[i + 1] : players[0]}",
          "user_color": DocData["turn"] == playerRef.data()?["uid"]
              ? Colors.amber
              : Colors.black,
          "image_url": playerRef.data()?["photo_url"],
          "absoluteX": DocData["positions"]
                  ["${playerRef.data()?["uid"]}_absoluteX"] *
              width,
          "absoluteY": DocData["positions"]
                  ["${playerRef.data()?["uid"]}_absoluteY"] *
              height,
          "screenX": (_playerList.containsKey("${playerRef.data()?["uid"]}") &&
                  _playerList["${playerRef.data()?["uid"]}"]
                      .containsKey("screenX"))
              ? _playerList["${playerRef.data()?["uid"]}"]["screenX"]
              : 0.0,
          "screenY": (_playerList.containsKey("${playerRef.data()?["uid"]}") &&
                  _playerList["${playerRef.data()?["uid"]}"]
                      .containsKey("screenY"))
              ? _playerList["${playerRef.data()?["uid"]}"]["screenY"]
              : 0.0,
          "tapX": (_playerList.containsKey("${playerRef.data()?["uid"]}") &&
                  _playerList["${playerRef.data()?["uid"]}"]
                      .containsKey("tapX"))
              ? _playerList["${playerRef.data()?["uid"]}"]["tapX"]
              : 0.0,
          "tapY": (_playerList.containsKey("${playerRef.data()?["uid"]}") &&
                  _playerList["${playerRef.data()?["uid"]}"]
                      .containsKey("tapY"))
              ? _playerList["${playerRef.data()?["uid"]}"]["tapY"]
              : 0.0,
        };
      }
      return docsRef.data();
    }

    Random random = new Random();
    int randomNumber = 0;
    return Scaffold(
        appBar: AppBar(
          title: Align(
            alignment: Alignment.center,
            child: IconButton(
              icon: Image.asset(
                  DocData["turn"] == FirebaseAuth.instance.currentUser!.uid
                      ? 'assets/dice2.png'
                      : 'assets/dice.png'),
              iconSize:
                  DocData["turn"] == FirebaseAuth.instance.currentUser!.uid
                      ? 50
                      : 30,
              onPressed: () async {
                if (DocData["turn"] == FirebaseAuth.instance.currentUser!.uid) {
                  randomNumber = random.nextInt(6) + 1;
                  var queryGame = await FirebaseFirestore.instance
                      .collection("game")
                      .doc(widget.event_id);

                  queryGame.update({"randomInt": randomNumber, "wait": true});
                  await Future.delayed(const Duration(seconds: 3), () {
                    queryGame.update({"wait": false});
                  });
                } else {
                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          title: const Text('Turns'),
                          content: SingleChildScrollView(
                            child: ListBody(
                              children: [
                                Text("It's not your turn"),
                                // }
                              ],
                            ),
                          ),
                          actions: [
                            TextButton(
                              onPressed: () {
                                Navigator.of(context, rootNavigator: true)
                                    .pop('dialog');
                              },
                              child: Text("Close"),
                            ),
                          ],
                        );
                      });
                }
              },
            ),
          ),
          leading: IconButton(
              onPressed: () {
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: const Text('Leave Game'),
                        content: SingleChildScrollView(
                          child: ListBody(
                            children: const <Widget>[
                              Text("Do you want to leave the game?"),
                            ],
                          ),
                        ),
                        actions: [
                          TextButton(
                            onPressed: () {
                              Navigator.pushAndRemoveUntil(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => LogScreen(),
                                  ),
                                  (r) => false);
                            },
                            child: Text("Leave"),
                          ),
                        ],
                      );
                    });
              },
              icon: Icon(Icons.exit_to_app)),
          actions: [
            IconButton(
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => Chat(
                          code: widget.event_id,
                        )));
              },
              icon: Icon(Icons.chat),
            ),
            // IconButton(
            //   onPressed: () async {
            //     print("Center players, if DM, centers all");
            //     for (var player in users_in_game) {
            //       if (isPlayerDM || player["uid"] == _currentPlayerId) {
            //         player["absoluteX"] =
            //             (MediaQuery.of(context).size.width / 2.0);
            //         player["absoluteY"] =
            //             (MediaQuery.of(context).size.height / 2.25);

            //         var queryGame = await FirebaseFirestore.instance
            //             .collection("game")
            //             .doc(widget.event_id);

            //         await queryGame.update({
            //           "positions.${player["id"]}_absoluteX":
            //               player["absoluteX"] / width,
            //           "positions.${player["id"]}_absoluteY":
            //               player["absoluteY"] / height,
            //         });

            //         setState(() {});
            //       }
            //     }
            //   },
            //   icon: Icon(Icons.center_focus_strong),
            // ),
          ],
        ),
        body: FutureBuilder(
            future: _asyncMethod(DocData["players"]),
            builder: (BuildContext context,
                AsyncSnapshot<Map<String, dynamic>?> snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return GameMapWait2(context, image, DocData["turn"]);
              } else {
                return SizedBox.expand(
                  child: InteractiveViewer(
                    key: GlobalKey(),
                    boundaryMargin: EdgeInsets.all(0),
                    minScale: 1,
                    maxScale: 2,
                    transformationController: _transformationController,
                    onInteractionEnd: (ScaleEndDetails details) {
                      _currentScaleValue =
                          _transformationController.value.getMaxScaleOnAxis();
                      print(
                          "Zoom actual: ${_transformationController.value.getMaxScaleOnAxis()}");
                    },
                    child: Stack(children: [
                      Stack(
                        children:
                            generate_board(_playerList, image, DocData["turn"]),
                      ),
                      if (DocData["wait"])
                        Align(
                            alignment: Alignment.center,
                            child: Text(
                              "${DocData["randomInt"]}",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 150,
                                  shadows: [
                                    Shadow(
                                      offset: Offset(10.0, 10.0),
                                      blurRadius: 3.0,
                                      color: Colors.black,
                                    ),
                                    Shadow(
                                        offset: Offset(10.0, 10.0),
                                        blurRadius: 8.0,
                                        color: Colors.black),
                                  ]),
                            ))
                    ]),
                  ),
                );
              }
            }));
  }

  Scaffold GameMapWait(BuildContext context, image, turn) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            onPressed: () {
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      title: const Text('Leave Game'),
                      content: SingleChildScrollView(
                        child: ListBody(
                          children: const <Widget>[
                            Text("Do you want to leave the game?"),
                          ],
                        ),
                      ),
                      actions: [
                        TextButton(
                          onPressed: () {
                            Navigator.pushAndRemoveUntil(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => LogScreen(),
                                ),
                                (r) => false);
                          },
                          child: Text("Salir"),
                        ),
                      ],
                    );
                  });
            },
            icon: Icon(Icons.exit_to_app)),
        title: Align(
          alignment: Alignment.center,
          child: IconButton(
            icon: Image.asset('assets/dice.png'),
            iconSize: 30,
            onPressed: () {
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      title: const Text('Turns'),
                      content: SingleChildScrollView(
                        child: ListBody(
                          children: const <Widget>[
                            Text("Is not your turn"),
                          ],
                        ),
                      ),
                      actions: [
                        TextButton(
                          onPressed: () {
                            Navigator.of(context, rootNavigator: true)
                                .pop('dialog');
                          },
                          child: Text("Close"),
                        ),
                      ],
                    );
                  });
            },
          ),
        ),
        actions: [
          IconButton(
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => Chat(
                        code: widget.event_id,
                      )));
            },
            icon: Icon(Icons.chat),
          ),
          // IconButton(
          //   onPressed: () {
          //     print("Center players, if DM, centers all");
          //     for (var player in users_in_game) {
          //       if (isPlayerDM || player["uid"] == _currentPlayerId) {
          //         player["absoluteX"] =
          //             (MediaQuery.of(context).size.width / 2.0);
          //         player["absoluteY"] =
          //             (MediaQuery.of(context).size.height / 2.25);
          //         setState(() {});
          //       }
          //     }
          //   },
          //   icon: Icon(Icons.center_focus_strong),
          // ),
        ],
      ),
      body: SizedBox.expand(
        child: InteractiveViewer(
          key: GlobalKey(),
          boundaryMargin: EdgeInsets.all(0),
          minScale: 1,
          maxScale: 2,
          transformationController: _transformationController,
          onInteractionEnd: (ScaleEndDetails details) {
            _currentScaleValue =
                _transformationController.value.getMaxScaleOnAxis();
            print(
                "Zoom actual: ${_transformationController.value.getMaxScaleOnAxis()}");
          },
          child: Stack(
            children: generate_board(_playerList, image, turn),
          ),
        ),
      ),
    );
  }

  Scaffold GameMapWait2(BuildContext context, image, turn) {
    return Scaffold(
      body: SizedBox.expand(
        child: InteractiveViewer(
          key: GlobalKey(),
          boundaryMargin: EdgeInsets.all(0),
          minScale: 1,
          maxScale: 2,
          transformationController: _transformationController,
          onInteractionEnd: (ScaleEndDetails details) {
            _currentScaleValue =
                _transformationController.value.getMaxScaleOnAxis();
            print(
                "Zoom actual: ${_transformationController.value.getMaxScaleOnAxis()}");
          },
          child: Stack(
            children: generate_board(_playerList, image, turn),
          ),
        ),
      ),
    );
  }

  List<Widget> generate_board(Map<String, dynamic> players, image, turn) {
    // Init variables
    List<Widget> boardElements = [];
    double appBarHeight = AppBar().preferredSize.height;
    double statusBarHeight = MediaQuery.of(context).viewPadding.top;
    Map<String, dynamic>? temp;
    // Create Widgets
    boardElements.add(game_board(image));
    for (var player in players.values) {
      // Si eres Dungeon Master puedes controlar todo, si no, solo puedes controlar tu pieza
      if ((isPlayerDM ||
              player["id"].replaceAll(' ', '') ==
                  _currentPlayerId.replaceAll(' ', '')) &&
          turn == FirebaseAuth.instance.currentUser!.uid) {
        if (!isPlayerDM) {
          // Esto es para asegurarnos de que la pieza controlable siempre este arriba
          temp = player;
        } else {
          boardElements.add(get_controllable_piece(
              player, appBarHeight, statusBarHeight, players));
        }
      } else {
        boardElements.add(
            get_non_controllable_piece(player, appBarHeight, statusBarHeight));
      }
    }
    if (temp != null) {
      boardElements.add(
          get_controllable_piece(temp, appBarHeight, statusBarHeight, players));
    }
    // End
    return boardElements;
  }

  Container game_board(image) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
            image: NetworkImage(
              image,
            ),
            fit: BoxFit.fill),
      ),
      width: double.infinity,
      height: double.infinity,
    );
  }

  GestureDetector game_piece(player) {
    double width = MediaQuery.of(context).size.width;
    return GestureDetector(
      onTap: () {
        showDialog(
            context: context,
            builder: (BuildContext build) {
              return AlertDialog(
                title: Text("${player["name"]}"),
                content: SingleChildScrollView(
                  child: ListBody(
                    children: <Widget>[
                      Text("Player information"),
                    ],
                  ),
                ),
              );
            });
      },
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(0.08 * width),
          border: Border.all(width: 2, color: player["user_color"]),
          image: DecorationImage(
              image: NetworkImage(
                player["image_url"],
              ),
              fit: BoxFit.contain),
        ),
        width: 0.08 * width,
        height: 0.08 * width,
      ),
    );
  }

  Positioned get_controllable_piece(Map<String, dynamic> player,
      double appBarHeight, double statusBarHeight, players) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Positioned(
      left: player["absoluteX"],
      top: player["absoluteY"],
      child: Draggable(
        feedback: game_piece(player),
        child: Listener(
          onPointerDown: (PointerDownEvent e) {
            // Tap pos
            // Sacar posicion de donde se hizo click el Draggable
            player["tapX"] = (e.position.dx - player["absoluteX"]);
            player["tapY"] = (e.position.dy -
                player["absoluteY"] -
                appBarHeight -
                statusBarHeight);
          },
          child: game_piece(player),
        ),
        onDragUpdate: (DragUpdateDetails details) {
          // Screen pos
          // Obtener posicion en pantalla
          player["screenX"] = (details.localPosition.dx);
          player["screenY"] =
              (details.localPosition.dy) - appBarHeight - statusBarHeight;
        },
        onDragEnd: (details) async {
          // Absolute pos
          // Actualizar posicion absoluta

          player["absoluteX"] = (player["absoluteX"] +
              ((player["screenX"] - player["absoluteX"] - player["tapX"]) /
                  _currentScaleValue));

          player["absoluteY"] = (player["absoluteY"] +
              ((player["screenY"] - player["absoluteY"] - player["tapY"]) /
                  _currentScaleValue));

          var queryGame = await FirebaseFirestore.instance
              .collection("game")
              .doc(widget.event_id);

          var actualPlayer =
              players["${FirebaseAuth.instance.currentUser!.uid}"];
          // print(actualPlayer);
          // print("${FirebaseAuth.instance.currentUser!.uid}");
          // print(players);
          await queryGame.update({
            "turn": actualPlayer["next_user"],
            "positions.${player["id"]}_absoluteX": player["absoluteX"] / width,
            "positions.${player["id"]}_absoluteY": player["absoluteY"] / height,
          });
          setState(() {});
        },
      ),
    );
  }

  Positioned get_non_controllable_piece(Map<String, dynamic> player,
      double appBarHeight, double statusBarHeight) {
    return Positioned(
        left: player["absoluteX"],
        top: player["absoluteY"],
        child: game_piece(player));
  }
}
