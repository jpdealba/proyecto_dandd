import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'ingame_event.dart';
part 'ingame_state.dart';

class IngameBloc extends Bloc<IngameEvent, IngameState> {
  IngameBloc() : super(IngameInitial()) {
    on<IngameEvent>((event, emit) {
      // TODO: implement event handler
    });
  }
}
