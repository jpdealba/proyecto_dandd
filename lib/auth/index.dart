import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:proyecto/LogIn/bloc/loginauth_bloc.dart';
import 'package:proyecto/auth/UserAuthRepository.dart';
import 'package:proyecto/auth/bloc/join_bloc.dart';
import 'package:proyecto/auth/join.dart';
import 'package:proyecto/createMenu/createMenu.dart';
import 'package:proyecto/gamePreview/game_preview.dart';
import 'package:proyecto/my_presets.dart';

import '../createMenu/bloc/picture_bloc.dart';

class LogScreen extends StatefulWidget {
  LogScreen({Key? key}) : super(key: key);

  @override
  State<LogScreen> createState() => _LogScreenState();
}

class _LogScreenState extends State<LogScreen> {
  @override
  Widget build(BuildContext context) {
    var currentUser = UserAuthRepository();
    var codeC = TextEditingController();
    return BlocConsumer<JoinBloc, JoinState>(
      listener: (context, state) {
        if (state is JoinSuccessState && codeC.text.isNotEmpty) {
          print("prueba1: ${codeC.text}");
          Navigator.of(context).push(MaterialPageRoute(
            builder: (context) =>
                JoinGame(code: codeC.text.toString(), game: state.game),
          ));
        } else if (state is JoinErrorState) {
          ScaffoldMessenger.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(SnackBar(content: Text("Invalid Code")));
        }
      },
      builder: (context, state) {
        return GestureDetector(
          onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
          child: Scaffold(
            appBar: AppBar(
                title: Align(
                    alignment: Alignment.center,
                    child: Text('Main Menu', style: appbar_text1())),
                actions: [
                  IconButton(
                    onPressed: () {
                      BlocProvider.of<LoginauthBloc>(context)
                          .add(SignOutEvent());
                    },
                    icon: Icon(
                      Icons.logout,
                      color: Colors.white,
                      size: 25.0,
                    ),
                  ),
                ]),
            body: SingleChildScrollView(
              child: Container(
                height: MediaQuery.of(context).size.height -
                    get_appbar_size() -
                    MediaQuery.of(context).viewPadding.top,
                decoration: container_style_2(),
                child: Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        ListTile(
                          leading: CircleAvatar(
                            radius: 30.0,
                            backgroundImage:
                                NetworkImage(currentUser.get_image()),
                          ),
                          title: Text("${currentUser.get_name()}",
                              style: TextStyle(
                                  fontSize: 25, fontWeight: FontWeight.bold)),
                        ),
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              primary: Colors.white,
                              minimumSize: Size(250.0, 75.0),
                              maximumSize: Size(250.0, 75.0)),
                          child: Text("Join Game", style: text_style_1()),
                          onPressed: () {
                            if (codeC.text.length > 15) {
                              BlocProvider.of<JoinBloc>(context)
                                  .add(JoinWithCodeEvent(
                                code: codeC.text,
                              ));
                            } else {
                              ScaffoldMessenger.of(context)
                                ..hideCurrentSnackBar()
                                ..showSnackBar(
                                    SnackBar(content: Text("Invalid Code")));
                            }
                          }, //function
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 35.0, right: 35),
                          child: TextFormField(
                            maxLength: 25,
                            controller: codeC,
                            textAlign: TextAlign.center,
                            style: textfield_style_1(),
                            decoration: const InputDecoration(
                              alignLabelWithHint: true,
                              labelStyle: TextStyle(
                                  fontSize: 50,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                              labelText: 'Enter Code',
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: Container(
                            child: Center(
                              child: Image.asset(
                                "assets/board-game.png",
                                color: Colors.white,
                              ),
                            ),
                            height: 50,
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage("assets/divider.png"),
                                  fit: BoxFit.fill),
                            ),
                          ),
                        ),
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              primary: Colors.white,
                              minimumSize: Size(250.0, 75.0),
                              maximumSize: Size(250.0, 75.0)),
                          child: Text("Create Game", style: text_style_1()),
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => BlocProvider(
                                        create: (context) => PictureBloc(),
                                        child: CreateMenu(),
                                      )),
                            );
                          }, //function
                        ),
                      ]),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
