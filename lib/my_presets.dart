import 'package:flutter/material.dart';

TextStyle text_style_1() {
  return TextStyle(
      fontSize: 40,
      color: Colors.black,
      fontWeight: FontWeight.bold,
      fontFamily: "Letter");
}

TextStyle text_style_2() {
  return TextStyle(
      fontSize: 30,
      color: Colors.black,
      fontWeight: FontWeight.bold,
      fontFamily: "Letter");
}

TextStyle textfield_style_1() {
  return TextStyle(
      fontSize: 35,
      color: Colors.black,
      fontWeight: FontWeight.bold,
      fontFamily: "Letter");
}

TextStyle appbar_text1() {
  return TextStyle(
    fontSize: 30,
    fontFamily: "Letter",
    fontWeight: FontWeight.bold,
  );
}

double get_appbar_size() {
  AppBar appBar = AppBar();
  return appBar.preferredSize.height;
}

BoxDecoration container_style_1() {
  return BoxDecoration(
    image: DecorationImage(
        image: AssetImage("assets/pieces_blue.png"), fit: BoxFit.cover),
    color: Colors.white,
  );
}

BoxDecoration container_style_2() {
  return BoxDecoration(
    image: DecorationImage(
        colorFilter: new ColorFilter.mode(
            Colors.black.withOpacity(0.1), BlendMode.dstATop),
        image: AssetImage("assets/pieces_white.png"),
        fit: BoxFit.fill),
    color: Colors.lightBlueAccent,
  );
}
