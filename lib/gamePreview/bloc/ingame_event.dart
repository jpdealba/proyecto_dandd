part of 'ingame_bloc.dart';

abstract class IngameEvent extends Equatable {
  const IngameEvent();

  @override
  List<Object> get props => [];
}
