// ignore_for_file: prefer_const_constructors

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:proyecto/gamePreview/game_preview.dart';
import 'package:http/http.dart' as http;
import 'package:proyecto/my_presets.dart';
import 'package:share_plus/share_plus.dart';
import '../chat.dart';

class WaitingScreen extends StatefulWidget {
  final String event_id;
  WaitingScreen({Key? key, required this.event_id}) : super(key: key);

  @override
  State<WaitingScreen> createState() => _WaitingScreenState();
}

class _WaitingScreenState extends State<WaitingScreen> {
  var font_size1 = 25;

  final FirebaseAuth _auth = FirebaseAuth.instance;
  var user;
  List<dynamic> users_in_game = [];
  List<String> ids = [];
  @override
  void initState() {
    super.initState();
    List<dynamic> new_users_in_game = [];
    List<String> new_ids = [];
    FirebaseFirestore.instance
        .collection('game')
        .doc(widget.event_id)
        .snapshots()
        .listen(
      (DocumentSnapshot documentSnapshot) async {
        new_ids = [];
        new_users_in_game = [];
        for (var i = 0; i < documentSnapshot["players"].length; i++) {
          var queryPlayer = await FirebaseFirestore.instance
              .collection("user")
              .doc("${documentSnapshot["players"][i]}");
          var playerRef = await queryPlayer.get();
          if (!new_ids.contains(playerRef.data()?["uid"])) {
            new_users_in_game.add(playerRef.data());
            new_ids.add(playerRef.data()?["uid"]);
          }
          users_in_game = new_users_in_game;
          ids = new_ids;
        }
      },
    ).onError((e) => print(e));
    setState(() {});
  }

  Future<void> shareCode(String code) async {
    //await Share.share(code);
  }

  @override
  Widget build(BuildContext context) {
    Stream _gameStream = FirebaseFirestore.instance
        .collection('game')
        .doc(widget.event_id)
        .snapshots(includeMetadataChanges: true);

    String _room_num = widget.event_id;
    return StreamBuilder<dynamic>(
        stream: _gameStream,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Text('Something went wrong');
          }
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: CircularProgressIndicator(),
            );
            ;
          } else {
            var DocData = snapshot.data as DocumentSnapshot;
            Future<Map<String, dynamic>?> _asyncMethod() async {
              var queryUser = await FirebaseFirestore.instance
                  .collection("user")
                  .doc("${FirebaseAuth.instance.currentUser!.uid}");
              var docsRef = await queryUser.get();
              user = docsRef.data();

              for (var i = 0; i < DocData["players"].length; i++) {
                var queryPlayer = await FirebaseFirestore.instance
                    .collection("user")
                    .doc("${DocData["players"][i]}");
                var playerRef = await queryPlayer.get();
                if (!ids.contains(playerRef.data()?["uid"])) {
                  users_in_game.add(playerRef.data());
                  ids.add(playerRef.data()?["uid"]);
                }
              }

              return docsRef.data();
            }

            return Scaffold(
                appBar: AppBar(
                  title: Align(
                    alignment: Alignment.center,
                    child: Text(
                      "Waiting Room",
                      style: appbar_text1(),
                    ),
                  ),
                  actions: <Widget>[
                    Padding(
                        padding: EdgeInsets.only(right: 20.0),
                        child: GestureDetector(
                          onTap: () async {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => Chat(
                                      code: widget.event_id,
                                    )));
                          },
                          child: Icon(
                            Icons.message,
                            size: 26.0,
                          ),
                        )),
                  ],
                ),
                body: SingleChildScrollView(
                  child: Container(
                    height: MediaQuery.of(context).size.height -
                        get_appbar_size() -
                        MediaQuery.of(context).viewPadding.top,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage("assets/pieces_blue.png"),
                          fit: BoxFit.fill),
                      color: Colors.white,
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(25.0),
                      child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                ListTile(
                                    title: Text(
                                      "Room Code: ${_room_num}",
                                      style: TextStyle(
                                          fontSize: 26,
                                          fontWeight: FontWeight.bold),
                                      textAlign: TextAlign.center,
                                    ),
                                    trailing: IconButton(
                                      onPressed: () async {
                                        //Share.share('${widget.event_id}',
                                        //    subject: 'Join My New Game In D&D');
                                      },
                                      icon: Icon(Icons.share,
                                          color: Colors.blueAccent),
                                      tooltip: "Share",
                                    )),
                                Text(
                                  "Map",
                                  style: TextStyle(
                                      fontSize: 35,
                                      fontWeight: FontWeight.bold),
                                  textAlign: TextAlign.center,
                                ),
                                Text(
                                  "${DocData["title"]}",
                                  style: TextStyle(
                                      fontSize: 30,
                                      fontWeight: FontWeight.bold),
                                  textAlign: TextAlign.center,
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 40,
                            ),
                            Center(
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                    shape: CircleBorder(),
                                    padding: EdgeInsets.all(45)),
                                onPressed: () async {
                                  var users_positions = DocData["positions"];
                                  double sum = 0.08;
                                  for (int i = 0;
                                      i < users_in_game.length;
                                      i++) {
                                    if (!users_positions.containsKey(
                                        "${users_in_game[i]["uid"]}_absoluteX")) {
                                      users_positions[
                                              "${users_in_game[i]["uid"]}_absoluteX"] =
                                          sum;
                                      users_positions[
                                              "${users_in_game[i]["uid"]}_absoluteY"] =
                                          0.0;
                                      sum += 0.089;
                                    }
                                  }
                                  var queryGame = await FirebaseFirestore
                                      .instance
                                      .collection("game")
                                      .doc(widget.event_id);
                                  await queryGame.update({
                                    "isStarted": true,
                                    "positions": users_positions
                                  });
                                  Navigator.of(context).pushReplacement(
                                      MaterialPageRoute(
                                          builder: (context) => GamePreview(
                                              event_id: widget.event_id)));
                                },
                                child: Text("Play!",
                                    style: TextStyle(fontSize: 50)),
                              ),
                            ),
                            SizedBox(
                              height: 50,
                            ),
                            Column(
                              children: [
                                Text(
                                  "Players",
                                  style: TextStyle(
                                      fontSize: 30,
                                      fontWeight: FontWeight.bold),
                                ),
                                FutureBuilder(
                                    future: _asyncMethod(),
                                    builder: (BuildContext context,
                                        AsyncSnapshot<Map<String, dynamic>?>
                                            snapshot) {
                                      if (snapshot.connectionState ==
                                          ConnectionState.waiting) {
                                        return CircularProgressIndicator();
                                      } else {
                                        return Container(
                                          width: double.infinity,
                                          height: 150,
                                          child: ListView.builder(
                                              scrollDirection: Axis.horizontal,
                                              itemCount: users_in_game.length,
                                              itemBuilder: (context, index) {
                                                return playerTile(
                                                    index, users_in_game);
                                              }),
                                        );
                                      }
                                    })
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ));
          }
        });
  }

  Padding playerTile(int index, var users_in_game) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: SingleChildScrollView(
        child: Center(
          child: Container(
            width: 150,
            height: 130.0,
            // decoration: BoxDecoration(
            //     color: Colors.pink, borderRadius: BorderRadius.circular(25)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Container(
                    width: 80.0,
                    height: 80.0,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                          fit: BoxFit.fill,
                          image: NetworkImage(
                              '${users_in_game[index]["photo_url"]}'),
                        ))),
                Text(
                  '${users_in_game[index]["username"]}',
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 23, fontWeight: FontWeight.bold),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
