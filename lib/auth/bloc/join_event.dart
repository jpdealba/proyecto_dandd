part of 'join_bloc.dart';

abstract class JoinEvent extends Equatable {
  const JoinEvent();

  @override
  List<Object> get props => [];
}

class JoinWithCodeEvent extends JoinEvent {
  final String code;

  JoinWithCodeEvent({required this.code});

  @override
  List<Object> get props => [];
}
