import 'dart:async';
import 'dart:io';
import 'package:bloc/bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:path_provider/path_provider.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter/services.dart' show rootBundle;

part 'picture_event.dart';
part 'picture_state.dart';

class PictureBloc extends Bloc<PictureEvent, PictureState> {
  File? _selectedPicture;
  PictureBloc() : super(PictureInitial()) {
    on<ChangeImageEvent>(_onChangeImage);
    on<ChangeImageCameraEvent>(_onCameraImage);
    on<PostGameEvent>(_onPostGame);
  }

  void _onCameraImage(ChangeImageCameraEvent event, Emitter emit) async {
    try {
      File? img = await _cameraImage();
      if (img != null) {
        _selectedPicture = img;
        emit(PictureSelectedState(picture: img));
      } else {
        emit(PictureErrorState(
            errorMsg: "No se pudo cargar imagen (Recibio nulo)"));
      }
    } catch (e) {
      emit(PictureErrorState(errorMsg: "No se pudo cargar imagen"));
    }
  }

  void _onChangeImage(ChangeImageEvent event, Emitter emit) async {
    try {
      File? img = await _pickImage();
      if (img != null) {
        _selectedPicture = img;
        emit(PictureSelectedState(picture: img));
      } else {
        emit(PictureErrorState(
            errorMsg: "No se pudo cargar imagen (Recibio nulo)"));
      }
    } catch (e) {
      emit(PictureErrorState(errorMsg: "No se pudo cargar imagen"));
    }
  }

  Future<File?> _cameraImage() async {
    final picker = ImagePicker();
    final XFile? chosenImage = await picker.pickImage(
      source: ImageSource.camera,
      maxHeight: 720,
      maxWidth: 720,
      imageQuality: 85,
    );
    return chosenImage != null ? File(chosenImage.path) : null;
  }

  Future<File?> _pickImage() async {
    final picker = ImagePicker();
    final XFile? chosenImage = await picker.pickImage(
      source: ImageSource.gallery,
      maxHeight: 720,
      maxWidth: 720,
      imageQuality: 85,
    );
    return chosenImage != null ? File(chosenImage.path) : null;
  }

  FutureOr<void> _onPostGame(PostGameEvent event, Emitter emit) async {
    emit(LoadGameState());
    String saved = await _saveGame(event);
    emit(saved != "" ? PostGameState(event_id: saved) : ErrorGameState());
  }

  Future<String> _uploadPictureToStorage() async {
    try {
      var stamp = DateTime.now();
      if (_selectedPicture == null) {
        return "https://firebasestorage.googleapis.com/v0/b/dnd-jpdealba.appspot.com/o/default_map.png?alt=media&token=48313438-32b0-4543-b471-ec83d5c779a8";
      }
      UploadTask task = FirebaseStorage.instance
          .ref("maps/imagen_$stamp.png")
          .putFile(_selectedPicture!);
      await task;
      return await (await task.storage)
          .ref("maps/imagen_$stamp.png")
          .getDownloadURL();
    } catch (e) {
      return "https://firebasestorage.googleapis.com/v0/b/dnd-jpdealba.appspot.com/o/default_map.png?alt=media&token=48313438-32b0-4543-b471-ec83d5c779a8";
    }
  }

  Future<String> _saveGame(PostGameEvent event) async {
    String imageUrl = await _uploadPictureToStorage();
    try {
      var resp = await FirebaseFirestore.instance
          .collection("game")
          .add({"title": event.title});
      print(resp);
      // //si se subio la imagen se acutaliza el mapa
      var queryGame =
          await FirebaseFirestore.instance.collection("game").doc(resp.id);

      await FirebaseFirestore.instance
          .collection("chat")
          .doc(resp.id)
          .set({"game_id": resp.id, "messages": []});

      await queryGame.update({
        "map": imageUrl,
        "code": resp.id,
        "height": event.height,
        "width": event.width,
        "title": event.title,
        "created_at": Timestamp.fromDate(DateTime.now()),
        "players": [FirebaseAuth.instance.currentUser!.uid],
        "enemies": [],
        "positions": {},
        "isStarted": false,
        "leader": FirebaseAuth.instance.currentUser!.uid,
        "turn": FirebaseAuth.instance.currentUser!.uid,
        "randomInt": 0,
        "wait": false
      });
      return resp.id;
    } catch (e) {
      return "";
    }
  }
}
