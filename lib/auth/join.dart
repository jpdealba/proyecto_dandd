import 'dart:collection';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:proyecto/chat.dart';

import '../gamePreview/game_preview.dart';
import '../my_presets.dart';
import 'bloc/join_bloc.dart';

class JoinGame extends StatefulWidget {
  final String code;
  final Map<String, dynamic>? game;
  JoinGame({Key? key, required this.code, required this.game})
      : super(key: key);

  @override
  State<JoinGame> createState() => _JoingameState();
}

class _JoingameState extends State<JoinGame> {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  var user;
  List<dynamic> users_in_game = [];
  List<String> ids = [];
  bool hasStarted = false;

  @override
  void initState() {
    super.initState();
    List<dynamic> new_users_in_game = [];
    List<String> new_ids = [];
    FirebaseFirestore.instance
        .collection('game')
        .doc(widget.code)
        .snapshots()
        .listen(
      (DocumentSnapshot documentSnapshot) async {
        new_ids = [];
        new_users_in_game = [];
        for (var i = 0; i < documentSnapshot["players"].length; i++) {
          var queryPlayer = await FirebaseFirestore.instance
              .collection("user")
              .doc("${documentSnapshot["players"][i]}");
          var playerRef = await queryPlayer.get();
          if (!new_ids.contains(playerRef.data()?["uid"])) {
            new_users_in_game.add(playerRef.data());
            new_ids.add(playerRef.data()?["uid"]);
          }
          users_in_game = new_users_in_game;
          ids = new_ids;
        }
      },
    ).onError((e) => print(e));
    setState(() {});
  }

  Future<Map<String, dynamic>?> _asyncMethod(players) async {
    var queryUser = await FirebaseFirestore.instance
        .collection("user")
        .doc("${FirebaseAuth.instance.currentUser!.uid}");
    var docsRef = await queryUser.get();
    user = docsRef.data();

    for (var i = 0; i < players.length; i++) {
      var queryPlayer = await FirebaseFirestore.instance
          .collection("user")
          .doc("${players[i]}");
      var playerRef = await queryPlayer.get();
      if (!ids.contains(playerRef.data()?["uid"])) {
        users_in_game.add(playerRef.data());
        ids.add(playerRef.data()?["uid"]);
      }
    }

    return docsRef.data();
  }

  @override
  Widget build(BuildContext context) {
    Stream _gameStream = FirebaseFirestore.instance
        .collection('game')
        .doc(widget.code)
        .snapshots(includeMetadataChanges: true);
    return StreamBuilder<dynamic>(
        stream: _gameStream,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Text('Something went wrong');
          }
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else {
            var DocData = snapshot.data as DocumentSnapshot;
            if (DocData["isStarted"] == true) {
              print("aqui");
              WidgetsBinding.instance?.addPostFrameCallback((_) =>
                  Navigator.of(context).pushReplacement(MaterialPageRoute(
                      builder: (context) =>
                          GamePreview(event_id: widget.code))));
            }
            return Scaffold(
              appBar: AppBar(
                title: Align(
                    alignment: Alignment.center,
                    child: Text(
                      "Game Lobby",
                      style: appbar_text1(),
                    )),
                actions: <Widget>[
                  Padding(
                      padding: EdgeInsets.only(right: 20.0),
                      child: GestureDetector(
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => Chat(
                                    code: widget.code,
                                  )));
                        },
                        child: Icon(
                          Icons.message,
                          size: 26.0,
                        ),
                      )),
                ],
              ),
              body: FutureBuilder(
                future: _asyncMethod(DocData["players"]),
                builder: (BuildContext context,
                    AsyncSnapshot<Map<String, dynamic>?> snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return CircularProgressIndicator();
                  } else {
                    return SingleChildScrollView(
                      child: Container(
                        decoration: container_style_1(),
                        height: MediaQuery.of(context).size.height,
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Column(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Align(
                                alignment: Alignment.topCenter,
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: ElevatedButton(
                                    onPressed: () {},
                                    child: CircleAvatar(
                                      backgroundColor: Colors.grey,
                                      radius: 80,
                                      backgroundImage: NetworkImage(
                                        user["photo_url"],
                                      ),
                                    ),
                                    style: ElevatedButton.styleFrom(
                                      shape: CircleBorder(),
                                    ),
                                  ),
                                ),
                              ),
                              Text(
                                "${_auth.currentUser!.displayName}",
                                style: TextStyle(
                                    fontSize: 28, fontWeight: FontWeight.bold),
                              ),
                              Text(
                                widget.game!["leader"] == _auth.currentUser!.uid
                                    ? "Role: Master"
                                    : "Role: Player",
                                style: TextStyle(
                                    fontSize: 28, color: Colors.blueGrey),
                              ),
                              SizedBox(height: 50),
                              Container(
                                width: 320,
                                decoration: BoxDecoration(
                                  border: Border.all(color: Colors.blue),
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.white,
                                ),
                                child: Column(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.all(10.0),
                                      child: Text(
                                        "Code",
                                        style: TextStyle(fontSize: 25),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(10.0),
                                      child: SelectableText(
                                        widget.code,
                                        toolbarOptions: ToolbarOptions(
                                            copy: true,
                                            selectAll: true,
                                            cut: false,
                                            paste: false),
                                        style: TextStyle(
                                          fontSize: 25,
                                          decoration: TextDecoration.underline,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(height: 50),
                              Padding(
                                padding: const EdgeInsets.all(5.0),
                                child: Align(
                                    alignment: Alignment.topLeft,
                                    child: Text('Users in game',
                                        style: TextStyle(
                                            fontSize: 30,
                                            fontWeight: FontWeight.bold))),
                              ),
                              Flexible(
                                child: ListView.builder(
                                  shrinkWrap: false,
                                  itemCount: users_in_game.length,
                                  scrollDirection: Axis.horizontal,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return Padding(
                                      padding: const EdgeInsets.all(5.0),
                                      child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 8.0),
                                              child: CircleAvatar(
                                                backgroundColor: Colors.grey,
                                                radius: 30,
                                                backgroundImage: NetworkImage(
                                                    '${users_in_game[index]["photo_url"]}'),
                                              ),
                                            ),
                                            Text(
                                              '${users_in_game[index]["username"]}',
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 24,
                                                  fontWeight: FontWeight.w500),
                                            ),
                                            Text(
                                              widget.game!["leader"] ==
                                                      users_in_game[index]
                                                          ["uid"]
                                                  ? "Role: Master"
                                                  : "Role: Player",
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 22,
                                                  fontWeight: FontWeight.bold),
                                            )
                                          ]),
                                    );
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  }
                },
                // child:
              ),
            );
          }
        });
  }

  void navigate(String code, BuildContext context) {
    Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (context) => GamePreview(event_id: code)));
  }
}
