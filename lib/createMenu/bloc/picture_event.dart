part of 'picture_bloc.dart';

abstract class PictureEvent extends Equatable {
  const PictureEvent();

  @override
  List<Object?> get props => [];
}

class ChangeImageEvent extends PictureEvent {}

class ChangeImageCameraEvent extends PictureEvent {}

class PostGameEvent extends PictureEvent {
  final String height;
  final String title;
  final String width;
  const PostGameEvent(
      {required this.height, required this.title, required this.width});

  @override
  List<Object?> get props => [];
}
