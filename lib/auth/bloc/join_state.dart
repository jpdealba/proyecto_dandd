part of 'join_bloc.dart';

abstract class JoinState extends Equatable {
  const JoinState();

  @override
  List<Map<String, dynamic>?> get props => [];
}

class JoinInitial extends JoinState {}

class JoinSuccessState extends JoinState {
  final Map<String, dynamic>? game;

  JoinSuccessState({required this.game});
  @override
  List<Map<String, dynamic>?> get props => [game];
}

class JoinErrorState extends JoinState {}

class JoinAwaitingState extends JoinState {}
