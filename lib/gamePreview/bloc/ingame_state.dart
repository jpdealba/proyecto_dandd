part of 'ingame_bloc.dart';

abstract class IngameState extends Equatable {
  const IngameState();

  @override
  List<Object> get props => [];
}

class IngameInitial extends IngameState {}

class IngameError extends IngameState {
  final String err;

  IngameError({required this.err});
  @override
  List<Object> get props => [err];
}

class IngameSuccess extends IngameState {}
